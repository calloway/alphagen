(ns alphagen.rules
  (:require [alphagen.board :refer [living? neighbors]]))
 

(defprotocol revive-rule
  (to-revive? [this board x y])
  (revive-dead [this board]))
  
(defprotocol kill-rule
  (to-die? [this board x y])
  (kill-living [this board]))

(defn- helper-now [this board val to-xxx?]
  (keep-indexed (fn [y ls] (keep-indexed (fn [x c]
                                          (if (to-xxx? this board x y)
                                            val
                                            c)) ls)) board))

(defn kill-now [this board]
  (helper-now this board 0 to-die?))
                                 
(defn revive-now [this board]
  (helper-now this board :revive to-revive?))
  
(deftype livingdies-rule []
  kill-rule
  (to-die? [this board x y]x
    (living?  board x y ))
  (kill-living [this board]
    (kill-now this board)))

(deftype underpopulation-rule []
  kill-rule
  (to-die? [this board x y]
    (let [n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (< (count living-neighbors) 2)))
  (kill-living [this board]
    (kill-now this board)))

(deftype overpopulation-rule []
  kill-rule
  (to-die? [this board x y]
    (let [n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (> (count living-neighbors) 3)))
  (kill-living [this board]
    (kill-now this board)))

(deftype reproduction-rule []
  revive-rule
  (to-revive? [this board x y]
    (let [dead? (not (living? board x y))
          n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (and dead? (== (count living-neighbors) 3))))
  (revive-dead [this board]
    (revive-now this board)))
  
