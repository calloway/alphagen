(ns alphagen.board)

(defn living? [board x y]
  (== 1 (get-in board [y x])))

(defn width [board] (count (first board)))
(defn height [board] (count (board)))

(defn to-neighbor-val [board x y]
  {:living (living? board x y) :pos [x y]})

(defn ne [board x y]
  (if-not (or (= x (dec (width board))) (zero? y))
    (to-neighbor-val board (inc x) (dec y))))  

(defn nw [board x y]
  (if-not (or (= x 0) (zero? y))
    (to-neighbor-val board (dec x) (dec y))))

(defn n [board x y]
  (if-not (zero? y)
    (to-neighbor-val board x (dec y))))
  
(defn s [board x y]
  (if-not (= (dec (count board)) y)
    (to-neighbor-val board x (inc y))))
 
(defn se [board x y]
  (if-not (or (= x (dec (count (first board)))) (= (dec (count board)) y))
    (to-neighbor-val board (inc x) (inc y)))) 
    
(defn sw [board x y]
  (if-not (or (zero? x) (= (dec (count board)) y))
    (to-neighbor-val board (dec x) (inc y))))
  
(defn e [board x y]
  (if-not (= (dec (count (first board))) x)
     (to-neighbor-val board (inc x) y)))

(defn w [board x y]
  (if-not (zero? x)
    (to-neighbor-val board (dec x) y)))

(defn neighbors [board x y]
  (remove nil? (list  (nw board x y) (ne board x y) (n board x y) (s board x y) (se board x y) (sw board x y) (w board x y) (e board x y))))
