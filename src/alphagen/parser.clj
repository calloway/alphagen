(ns alphagen.parser
  (:require [clojure.java.io :refer [resource]]
            [clojure.string :refer [split]]))


(defprotocol Parser
  (parse [this file]))

(defn- read-coord 
  "Given string represent. [[ '-1', '0']] --> output to int [[-1, 0]]"
  [[x y]]
  [(read-string x) (read-string y)])

(defn- examine-coords
  "Examines a list of xy coords and returns the boundaries of x and y"
  [ls] 
  (letfn [(firstvalx [ls] (ffirst ls))
          (firstvaly [ls] (second (first ls)))]
    (loop [ls ls minx (firstvalx ls) maxx (firstvalx ls) 
           miny (firstvaly ls) maxy (firstvaly ls)]
      (if (empty? ls)
        [[minx maxx] [miny maxy]]
        (recur (rest ls) 
               (min minx (firstvalx ls))
               (max maxx (firstvalx ls))
               (min miny (firstvaly ls))
               (max maxy (firstvaly ls)))))))

(defn- create-board
  "Creates a board given boundaries and coordinates.  Dead is 0.  Living is 1.

   2 inputs
   list containing boundaries of minx,max and miny, maxy
   a set of living coordinates #{[0 0]} would be one living coord
"
  [[[minx maxx] [miny maxy]] livingcoords]
  (into [] (for [y (range miny (inc maxy) 1)]
             (into [] (for [x (range minx (inc maxx) 1)]   
                        (if (livingcoords [x y]) 1 0))))))

(create-board [[-1 1] [-1 1]] #{ [0 0]})

(defn alphagen-parser []
  "Parses input files of format alphagen provided.  IE space delim x y
Returns a 2 dim vector of 0's and 1's.  [[0 1 0] [0 0 0]]  has one live"
  (reify Parser
    (parse [this file]
      (let [lines (split (slurp file) #"\n")
            data (filter #(not (re-find #"^#.*" %)) lines)
            livingcoords (into #{} (map read-coord (map #(split % #"\t") data)))
            boundaries (examine-coords livingcoords)
            board (create-board boundaries livingcoords)
            ]
        {:board board :boundaries boundaries}))))

;(parse (alphagen-parser) (resource "input.txt"))
;(examine-coords (parse (alphagen-parser) (resource "input.txt")))

