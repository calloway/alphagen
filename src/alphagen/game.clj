(ns alphagen.game
  (:require [clojure.string :refer [join]] 
            [clojure.java.io :refer [resource]]
            [clojure.math.numeric-tower :refer [abs sqrt expt]]
            [alphagen.board :refer :all]
            [alphagen.rules :refer :all]
            [alphagen.parser :refer :all])
  (:import [alphagen.rules overpopulation-rule underpopulation-rule reproduction-rule]))


(def deathrules (list (overpopulation-rule.) (underpopulation-rule.)))
(def reviverules (list (reproduction-rule.)))

(defn combine-results [result op]
  (reduce (fn[m1 m2] (map (fn [l1 l2] (map op l1 l2)) m1 m2)) result))

(defn- to-vec [nestedls]
  (into [] (map (fn [row] (into [] row)) nestedls)))

(defn evolve [{board :board boundaries :boundaries}]  
  {:boundaries boundaries
   :board  (to-vec  (combine-results (cons  (combine-results (map (fn [rule] (kill-living rule board)) deathrules) bit-and)
                                            (list (combine-results (map (fn [rule] (revive-dead rule board)) reviverules) bit-or))) 
                                     (fn [a b] (if (= :revive b) 1 a))))})

(defn print-board [board]
  (println (join "\n"  (map #(apply str %) (board :board)))))

(defn living-only [{board :board [[minx max] [miny maxy]] :boundaries}] 
  (filter #(= (% :val) 1) (flatten (keep-indexed (fn [y row]
                                                   (keep-indexed (fn [x val] {:x (+ minx x) :y (+ miny y) :val val}) row)) board))))

(defn print-living-only [board]
  (println (join "\n" (map #(apply str %) 
                           (map (fn [{x :x y :y}] 
                                  `(~x "\t" ~y))
                                (living-only board))))))


(defn -main [& args]
  (let [parseargs (fn [[num human-readable]] 
                    [(if (nil? num) 1 (read-string num)) human-readable])
        [numgenerations human-readable] (parseargs args)]
    (loop [i 0 board (parse (alphagen-parser) (resource "input.txt"))]
      (when (<= i numgenerations)
        (println "Board on iteration " i)
        (if human-readable
          (print-board board)
          (print-living-only board))
        (recur (inc i) (evolve board))))))
















