(ns alphagen.parser-test
  (:require [clojure.java.io :refer [resource]]
            [clojure.test :refer :all]
            [alphagen.parser :refer :all]))

(deftest alphagen-parser-test
  (testing "tests alphagen-parser"
    (is (= {:boundaries [[-1 1] [-1 1]] :board [[1 1 1] [0 0 1] [0 1 0]]} ( parse (alphagen-parser) (resource "input.txt"))))))
