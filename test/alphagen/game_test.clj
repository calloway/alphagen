(ns alphagen.game-test
  (:require [clojure.java.io :refer [resource]]
            [clojure.test :refer :all]
            [alphagen.game :refer :all]
            [alphagen.parser :refer :all]))

(deftest game-test
  (testing "tests alphagen-parser"
    (is (= {:boundaries [[-1 1] [-1 1]]
            :board  [[0 0 0] [0 1 1] [0 1 1]]} (evolve {:boundaries [[-1 1] [-1 1]] :board [[0 0 0] [0 1 1] [0 1 1]]})))))
